# Devcontainer ❤️  Neovim

A simple startup-script for installing and using [Neovim](https://neovim.io/)
in a [Devcontainer](https://containers.dev/).

## How it works

The startup-script uses [devcontainers-cli](https://github.com/devcontainers/cli)
to

1. build the devcontainer
2. install Neovim inside the container
3. install (git clone) your Neovim configuration inside the container
4. exec Neovim inside the container

## Requirements

### Host

* [bash](https://www.gnu.org/software/bash/)
* [jq](https://jqlang.github.io/jq/)
* [podman](http://podman.io/) or [docker](https://www.docker.com/)
* [devcontainers-cli](https://github.com/devcontainers/cli)

### Container

To keep the containers memory footprint as small as possible no additional
dependencies are installed. If your neovim configuration required additional
applications/libraries you have install them **separately**!

***Note:*** Additional dependencies could be installed via `devcontainer.json` `postCreateCommand`

## How to use

Just call `n-container-vim` and pass the root of your workspace:

```bash
n-container-vim path/to/my/project/root
```

### Configuration

For configuration the `n-container-vim` script sources environment variables
from `$HOME/.config/n-container-vim.env`.

| Variable Name | Description |
| - | - |
| CONTAINER_TOOL | select between `podman` (default) and `docker` |
| NVIM_CONFIG_GIT | url to git repository containing your neovim configuration |
| SSH_AGENT_S | file containing the output of [`ssh-agent -s`](https://code.visualstudio.com/remote/advancedcontainers/sharing-git-credentials#_using-ssh-keys) |

***Note:*** Further, undocumented variables can be found at start of script.
