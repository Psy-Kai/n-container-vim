#!/bin/bash

set -e

curl -LO https://github.com/neovim/neovim/releases/${NVIM_VERSION}/download/nvim-linux-x86_64.tar.gz
tar -C /opt -xzf nvim-linux-x86_64.tar.gz
rm nvim-linux-x86_64.tar.gz

if [ "${NVIM_CONFIG}" ]; then
	su ${_REMOTE_USER} -c "git clone ${NVIM_CONFIG} ${_REMOTE_USER_HOME}/.config/nvim"
fi
